package com.example.Ejercicio4_Tienda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ejercicio4TiendaApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ejercicio4TiendaApplication.class, args);
	}
}
