package com.example.Ejercicio4_Tienda.visual;

import com.example.Ejercicio4_Tienda.dominio.Producto;
import com.example.Ejercicio4_Tienda.dominio.ProductoRepository;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Baldemar on 21/07/2017.
 */
@SpringUI
public class App extends UI {
    @Autowired
    ProductoRepository productoRepository;

    @Override
    protected void init (VaadinRequest vaadinRequest){
        VerticalLayout layout = new VerticalLayout();
        HorizontalLayout hlayout = new HorizontalLayout();

        Label Productos = new Label("Ingresar Productos");
        TextField nombre = new TextField("Nombre");
        TextField precio = new TextField("Precio");
        TextField descrip = new TextField("Descripcion");


        Grid<Producto> grid = new Grid<>();
        grid.addColumn(Producto::getId).setCaption("ID");
        grid.addColumn(Producto::getNombrep).setCaption("Nombre");
        grid.addColumn(Producto::getPreciop).setCaption("Precio");
        grid.addColumn(Producto::getDescp).setCaption("Descripcion");


        Button add = new Button("Adicionar");
        add.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Producto p = new Producto();
                p.setNombrep(nombre.getValue());
                p.setPreciop(Double.parseDouble(precio.getValue()));
                p.setDescp(descrip.getValue());
                Notification.show("Producto Agregado");

                productoRepository.save(p);
                grid.setItems(productoRepository.findAll());

                nombre.clear();
                precio.clear();
                descrip.clear();
            }
        });

        layout.addComponents(Productos);
        hlayout.addComponents(nombre,precio,descrip,add);
        hlayout.setComponentAlignment(add, Alignment.BOTTOM_RIGHT);

        layout.addComponents(hlayout);
        layout.addComponents(grid);

        setContent(layout);
    }
}
