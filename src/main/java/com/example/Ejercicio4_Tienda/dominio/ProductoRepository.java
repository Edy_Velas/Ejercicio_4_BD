package com.example.Ejercicio4_Tienda.dominio;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Baldemar on 21/07/2017.
 */
public interface ProductoRepository extends JpaRepository<Producto,Long> {
}